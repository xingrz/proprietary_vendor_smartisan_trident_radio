DEVICE := trident

MODEM_IMG := firmware-update/modem.img
ABL_IMG := firmware-update/abl.img
XBL_IMG := firmware-update/xbl.img
XBL_CONFIG_IMG := firmware-update/xbl_config.img

TIMESTAMP := $(shell strings $(MODEM_IMG) | grep -m 1 '"Time_Stamp"' | sed -n 's|.*"Time_Stamp": "\([^"]*\)"|\1|p')
VERSION := $(shell echo $(TIMESTAMP) | sed 's|[ :-]*||g')

HASH_ABL := $(shell openssl dgst -r -sha1 $(ABL_IMG) | cut -d ' ' -f 1)
HASH_XBL := $(shell openssl dgst -r -sha1 $(XBL_IMG) | cut -d ' ' -f 1)
HASH_XBL_CONFIG := $(shell openssl dgst -r -sha1 $(XBL_CONFIG_IMG) | cut -d ' ' -f 1)

TARGET := RADIO-$(DEVICE)-$(VERSION).zip

# Build
# ==========

.PHONY: build
build: assert inspect $(TARGET)
	@echo Size: $(shell stat -f %z $(TARGET))

$(TARGET): META-INF firmware-update
	zip -r9 $@ $^

# Clean
# ==========

.PHONY: clean
clean:
	rm -f *.zip

# Assert
# ==========
.PHONY: assert
assert: $(ABL_IMG) $(XBL_IMG) $(XBL_CONFIG_IMG)
ifneq ($(HASH_ABL), a73368fd5d1a1e2eaf5dbf62d5f8cc5951cf6182)
	$(error SHA-1 of abl.img mismatch)
endif
ifneq ($(HASH_XBL), ac25a8d32d53537fb14ed2be997cd55711ce19e1)
	$(error SHA-1 of xbl.img mismatch)
endif
ifneq ($(HASH_XBL_CONFIG), cc6722ea51ddedd4b5c3038dad5c5ce2385ee0f0)
	$(error SHA-1 of xbl_config.img mismatch)
endif
	@echo Everything is ok.

# Inspect
# ==========

.PHONY: inspect
inspect: $(MODEM_IMG)
	@echo Target: $(TARGET)
	@echo Timestamp: $(TIMESTAMP)
